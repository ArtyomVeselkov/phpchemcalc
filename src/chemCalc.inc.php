<?php

/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/

/**
 * @author optimLight
 * @copyright 2014
 */
 
/*if (!defined('CHEMMODULEAPI'))
    exit('No direct script access allowed');*/

// script link
define('CFG_SELF_LINK', '058-ph-calculator.php');
// protocol
define('CFG_HTTP_PROTOCOL', 'http');
// site's domain
define('CFG_DOMAIN_NAME', 'chem-space.pp.ua');
// array of allowed comains
define('CFG_ALLOWED_DOMAINS', 'CFG_ALLOWED_DOMAINS');
// allowed GET access
define('CFG_DOMAIN_ALLOWED_GET', 'CFG_DOMAIN_ALLOWED_GET');
// allowed POST access
define('CFG_DOMAIN_ALLOWED_POST', 'CFG_DOMAIN_ALLOWED_POST');
// if it is right server - TRUE
define('CFG_ALLOWED_CALC', 'CFG_ALLOWED_CALC');
// a relative path to a work directory of phpChemCalc
define('CFG_PCC_RELPATH', 'online-services');
// a subfolder in CFG_PCC_RELPATH directory
define('CFG_STATIC_RELPATH', 'static');
// if script is running on GAE
define('CFG_GAE_FLAG_BUCKET', FALSE);
// relative or full path
define('CFG_USE_RELATIVE_PATH', TRUE);
if (defined('CFG_GAE_FLAG_BUCKET') && (TRUE == CFG_GAE_FLAG_BUCKET)) {
	// path to bucket if hosted on GAE	
	define('CFG_PCC_FULLPATH', 'gs' . '://' . CFG_DOMAIN_NAME . '/' . CFG_PCC_RELPATH . '/');
} else {
    if ( TRUE == CFG_USE_RELATIVE_PATH ) {
        define('CFG_PCC_FULLPATH', CFG_PCC_RELPATH . '/');
    } else {
        define('CFG_PCC_FULLPATH', CFG_HTTP_PROTOCOL . '://' . CFG_DOMAIN_NAME . '/' . CFG_PCC_RELPATH . '/');
    }
}
// look into chemCalc_DB.php for details
define('CFG_USE_AUTOPATH', TRUE);


// $_CHEM_CALC_RT_DATA -- data for current request calculation
// GET or POST -- already defined
// define('CHEM_API_MODE', 'CHEM_API_MODE');
define('CHEM_API_MODE_GET', 'CHEM_API_MODE_GET');
define('CHEM_API_MODE_POST', 'CHEM_API_MODE_POST');

// $_GET['CHEM_API_MODE']
define('CHEM_API_MODE', 'CHEM_API_MODE');
    
// $_POST["CHEM_API_ACTION"]
define('CHEM_API_ACTION', 'CHEM_API_ACTION');
// Calculation finished
define('CHEM_API_CALC_FIN', 'CHEM_API_CALC_FIN');
/**
 * 0  - not finished
 * 1  - ok
 * -1 - error
 */

// supported "actions"
define('CHEM_API_PH', "CHEM_API_PH");
define('CHEM_API_SEARCHABKD', "CHEM_API_SEARCHABKD");

/* required data for each calculation */
// CHEM_API_PH
define('CHEM_API_PH_CONCENTRATION', 'CHEM_API_PH_CONCENTRATION');
define('CHEM_API_PH_MODE', 'CHEM_API_PH_MODE');
define('CHEM_API_PH_FORMULA', 'CHEM_API_PH_FORMULA');
define('CHEM_API_PH_KW', 'CHEM_API_PH_KW');
define('CHEM_API_PH_OUTPUTMODE', 'CHEM_API_PH_OUTPUTMODE');
define('CHEM_API_PH_MAXITER', 'CHEM_API_PH_MAXITER');
define('CHEM_API_PH_KD1', 'CHEM_API_PH_KD1');
define('CHEM_API_PH_KD2', 'CHEM_API_PH_KD2');
define('CHEM_API_PH_KD3', 'CHEM_API_PH_KD3');
define('CHEM_API_PH_USEOWN', 'CHEM_API_PH_USEOWN');
// const for key in array with output data (depends on API_ACTION)
define('CHEM_API_OUTPUT_PH_DATA', 'CHEM_API_OUTPUT_PH_DATA');
/**
 * PH
 *   [0]  - status: -1 error, 0 proccess, 1 ok-fin
 *   [1]  - brutto
 *   [2]  - compound name
 *   [3]  - 
 *   [4]  - Kd1
 *   [5]  - Kd2  
 *   [6]  - Kd3
 *   [7]  - Kd4
 *   [8]  - temperature
 *   [9]  - literature 
 *   [10] - concentration
 *   [11] - pH
 *   [12] - pOH
 *   [13] - [H+]
 *   [14] - [OH-]
 * 
 */

// CHEM_API_SEARCHABKD
define('CHEM_API_SEARCHABKD_STRING', 'CHEM_API_SEARCHABKD_STRING');
define('CHEM_API_SEARCHABKD_MAXN', 'CHEM_API_SEARCHABKD_MAXN');
define('CHEM_API_SEARCHABKD_CASESEN', 'CHEM_API_SEARCHABKD_CASESEN');
define('CHEM_API_SEARCHABKD_REGEXP', 'CHEM_API_SEARCHABKD_REGEXP');
define('CHEM_API_SEARCHABKD_OUTPUTMODE', 'CHEM_API_SEARCHABKD_OUTPUTMODE');
define('CHEM_API_SEARCHABKD_STYLE', 'CHEM_API_SEARCHABKD_STYLE');
define('CHEM_API_SEARCHABKD_TDITEM_TEXT', 'CHEM_API_SEARCHABKD_TDITEM_TEXT');
define('CHEM_API_SEARCHABKD_TRITEM_ATTR', 'CHEM_API_SEARCHABKD_TRITEM_ATTR');
define('CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT', 'CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT');
define('CHEM_API_SEARCHABKD_TRLASTTEXT', 'CHEM_API_SEARCHABKD_TRLASTTEXT');

// common costs
define('CHEM_API_ARRAY_TABLE', 'tablePath');
define('CHEM_API_ARRAY_SOURCES', 'srcPath');
define('CHEM_API_ARRAY_LANG_RUS', 'Lang_rus');

define('CHEM_API_SEARCHABKD_LANG_NAME', 'name');
define('CHEM_API_SEARCHABKD_LANG_FORMULA', 'formula');
define('CHEM_API_SEARCHABKD_LANG_TEMP', 'temp');
define('CHEM_API_SEARCHABKD_LANG_SOURCE', 'source');
define('CHEM_API_SEARCHABKD_LANG_NOTES', 'notes');
define('CHEM_API_SEARCHABKD_LANG_RESULTS', 'res');
define('CHEM_API_SEARCHABKD_LANG_NOTALLRESULTS', 'notallres');
			

$_CHEM_CALC_SETTINGS = array(
                                CFG_ALLOWED_DOMAINS => array(
                                                                'chem-space.pp.ua' => array(
                                                                                            CFG_DOMAIN_ALLOWED_POST =>TRUE,
                                                                                            CFG_DOMAIN_ALLOWED_GET => TRUE
                                                                                           ),
                                                                'chem-space.appspot.com' => array(
                                                                                            CFG_DOMAIN_ALLOWED_POST => TRUE,
                                                                                            CFG_DOMAIN_ALLOWED_GET => TRUE
                                                                                                 )
                                                            )
                            );
                            
$_CHEM_CALC_RT_DATA = array(
                                CFG_ALLOWED_CALC => FALSE,
                                CHEM_API_MODE => '',
                                CHEM_API_ACTION => '',
                                CHEM_API_CALC_FIN => 0,
                                CHEM_API_OUTPUT_PH_DATA => array()
                           );
                            
$chemDB_tables = array(
						CHEM_API_PH => array(
                                   CHEM_API_ARRAY_TABLE => "chemTable_DissocConsts.csv",
                                   CHEM_API_ARRAY_SOURCES => "static/chemTable_DissocConsts_sourses.csv",
                                   CHEM_API_ARRAY_LANG_RUS => array(
								   )
                        ),
						CHEM_API_SEARCHABKD => array(
									CHEM_API_ARRAY_LANG_RUS => array(
										CHEM_API_SEARCHABKD_LANG_NAME => "Название",
										CHEM_API_SEARCHABKD_LANG_FORMULA => "Формула",
										CHEM_API_SEARCHABKD_LANG_TEMP => "Температура",
										CHEM_API_SEARCHABKD_LANG_SOURCE => "Источник",
										CHEM_API_SEARCHABKD_LANG_NOTES => "Заметки", 
										CHEM_API_SEARCHABKD_LANG_RESULTS => " результат(ов). Максимальное количество - ",
										CHEM_API_SEARCHABKD_LANG_NOTALLRESULTS => "Показаны не все результаты. Уточните поисковый запрос.",
                                        CHEM_API_SEARCHABKD_LANG_SELECT => "Это соединение",
                                        CHEM_API_SEARCHABKD_LANG_SELECT_HEADER => "Кликнуть для выбора"
									)
						)
                      );
?>