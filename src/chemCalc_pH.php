<?php

if (!defined('CHEMMODULEAPI'))
    exit('No direct script access allowed');
	
/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/

/**
 * ChemModule: Calculate conentration of \f$H_{3}O^{+}\f$ and \f$OH^{-}\f$ ions 
 * 
 * @author optimLight
 * @copyright 2012
 */

/**
 * Solve quadratic equation \f$a \cdot x^{2} + b \cdot x + c = 0\f$. Only real number for now
 * 
 * @param float $a 
 * @param float $b
 * @param float $c
 * @param float $x1 
 * @param float $x2
 * @return float if \f$D >= 0\f$ - returns x1, else if D<0 - return -1
 */
function calc_Quadratic($a, $b = 0.0, $c = 0.0, $x1 = -1.0, $x2 = -1.0)
{
    $D = $b * $b - 4 * $a * $c;
    $x1 = (-$b + sqrt($b * $b - 4 * $a * $c)) / (2 * $a);
    $x2 = (-$b - sqrt($b * $b - 4 * $a * $c)) / (2 * $a);
    return $D >= 0 ? $x1 : -1;
}


/**
 * Calculate the concentration of \f$H_{3}O^{+}\f$ or \f$OH^{-}\f$ ions by common formula, 
 * based on electroneutrality equation and material balance equation  
 * 
 * @param double $с concentration of substance or ion
 * @param double $Kd1 first dissociation constant
 * @param double $Kd2 second dissociation constant or 0.0
 * @param double $Kd3 third  dissociation constant or 0.0
 * @param string $data should be empty ('') - returns the addidional info such as time, iterations, ...
 * @param double $Kw autoprotolysis constant or default 1E-14 for water 
 * @param double $E Observational error (accuracy)
 * @param integer $maxIter maximum number of iterations
 * @param double $CStart start from this minimal concentration of \f$H_{3}O^{+}\f$ or \f$H^{+}\f$ ions
 * @param double $CEnd maximum possible value for concentration of \f$H_{3}O^{+}\f$ or \f$H^{+}\f$ ions
 * @return concentration of \f$H_{3}O^{+}\f$ or \f$OH^{-}\f$ ions
 */
function chemCalc_concentrationHOH_commonF($c, $Kd1, $Kd2 = 0.0, $Kd3 = 0.0, &$data = '', $Kw = 1.0E-13, $E = 1.0E-15, $maxIter =
    150, $CStart = 1.0E-24, $CEnd = 1.0E+3)
{
    $start = microtime(true);

    if ($E < 1E-15)
    {
        $E = 1E-15;
    }
    if ($maxIter > 150)
    {
        $maxIter = 150;
    }
    if ($CStart < 1E-24)
    {
        $CStart = 1E-24;
    }
    if ($CEnd > 1E+3)
    {
        $CEnd = 1E+3;
    }
    $maxTime = 5;

    $k0 = 1;
    $k1 = $Kd1;
    $k2 = ($Kd1 * $Kd2) - $Kw - ($c * $Kd1);
    $k3 = ($Kd1 * $Kd2 * $Kd3) - ($Kd1 * $Kw) - ($c * $Kd1 * $Kd2);
    $k4 = ($Kd1 * $Kd2 * $Kd3 * $c) - ($Kd1 * $Kd2 * $Kw);
    $k5 = (-1) * $Kd1 * $Kd2 * $Kd3 * $Kw;

    $iter = 0;
    $x = $CEnd;

    // if (f($x) * d2f(x) < 0)
    if ((($k0 * pow($x, 5)) + ($k1 * pow($x, 4)) + ($k2 * pow($x, 3)) + ($k3 * pow($x, 2)) + ($k4 * $x) + $k5) * ((20 * $k0 *
        pow($x, 3)) + (12 * $k1 * pow($x, 2)) + (6 * $k2 * $x) + (2 * $k3)) < 0)
    {
        $x = $CStart;
    }

    do
    {
        // $h = f($x) / df(x);
        $h = (($k0 * pow($x, 5)) + ($k1 * pow($x, 4)) + ($k2 * pow($x, 3)) + ($k3 * pow($x, 2)) + ($k4 * $x) + $k5) / ((5 * $k0 *
            pow($x, 4)) + (4 * $k1 * pow($x, 3)) + (3 * $k2 * pow($x, 2)) + (2 * $k3 * $x) + $k4);
        $x = $x - $h;
        $iter++;
    } while ((abs($h) > $E));
    /* || ($maxIter <= $iter)) || ((microtime(true) - $start) <= $maxTime)) */

    $time = microtime(true) - $start;
    $data = 'Accuracy: ±' . $E . ', time: ' . $time . ', iterations: ' . $iter . '(max ' . $maxIter . '), f(x) = ' . ' ' .
        ', f(x)/df(x) = ' . $h;
    return $x;
}

/**
 * Use equation \f$K = c \cdot \frac{ h^{2} }{ ( 1 - h ) }\f$ to calculate one of values K, c, h
 * 
 * @param double $K dissociation constant
 * @param double $c concentration
 * @param double $a or h. The degree of dissociation
 * @return double expected K, c or h. If fails - returns -1
 */
function chemCalc_OstwaldDilution($K = -1.0, $c = -1.0, $a = -1.0)
{
    if ((-1 == $K) && ($c > 0) && ($a > 0))
    {
        return $c * $a * $a / (1 - $a);
    }

    if (($c == -1) && ($K > 0) && ($a > 0))
    {
        return $K * (1 - $a) / $a * $a;
    }

    if ((-1 == $a) && ($c > 0) && ($K > 0))
    {
        return calc_Quadratic($c, $K, $K * (-1));
    }

    return - 1;
}

/**
 * Calculate concentration of \f$H_{3}O^{+}\f$ or \f$OH^{-}\f$ ions by simplified formula for each system:
 * 1, 2: strong acid/base with[out] autoprotolysis
 * 3: weak acid/base with h > 5% ?autoprotolysis
 * 4, 5: weak acid/base with $h < 5%\ with[out] autoprotolysis
 * 
 * @param mixed $c concentration of substance
 * @param mixed $Kd dissociation constant
 * @param integer $Kw autoprotolysis constant 
 * @return concentration of \f$H_{3}O^{+}\f$ or \f$OH^{-}\f$ ions OR (-1) 
 */
function chemCalc_concentrationHOH_simplifiedF($c, $Kd, $Kw = 1.0E-13)
{
    $M = -1;

    if ($Kd >= 1)
    {
        $M = $c >= 1.0E-05 ? 1 : 2;
    } else
    {
        if (chemCalc_OstwaldDilution($Kd, $c) > 0.05)
        {
            $M = 5;
        } else
        {
            $M = $c >= 1.0E-03 ? 3 : 4;
        }
    }

    switch ($M)
    {
        case 1: // strong acid, without "autoprotolysis"
            {
                return $c;
                break;
            }
        case 2: // strong acid, with "autoprotolysis"
            {
                return (($c + sqrt(pow($c, 2) + 4 * $Kw)) * 0.5);
            }
        case 3: // weak acid, h < 5%, without "autoprotolysis"
            {
                return (sqrt($Kd * $c));
                break;
            }
        case 4: // weak acid, h < 5%, with "autoprotolysis"
            {
                return (sqrt(($Kd * $c) + $Kw));
                break;
            }
        case 5: // weak acid, h > 5%, ???with "autoprotolysis"
            {
                return (((-1) * $Kd + sqrt(pow($Kd, 2) + 4 * $Kd * $c)) * 0.5);
                break;
            }
    }
    return - 1;
}

?>