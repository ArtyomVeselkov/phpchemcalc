/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov 
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/

function pcc_strAttention(str) {
	return '<font color="red">' + str + '</font>';
}

function pcc_prepareCalc() {
	var errors = 0;
	/* - - - - - - - -  - - - - - */
	// Check PreviewData block
	
	// :TODO: extra check if node doen't exisits
	var pd = document.getElementById('peviewData');
	var name = document.getElementById('subname');	
	if (name.innerText.length < 2) {
		errors++;
		name.innerHTML = strAttention('Минимальная длина строки поиска соединения - 2 символа');
	}
	var formula = document.getElementById('subf');
	if ('' == formula.innerText) {
		errors++;
		name.innerHTML = strAttention('Отсутствует формула соединения');
	}
	var kd =  document.getElementById('subKd');
	if ('' == formula.innerText) {
		errors++;
		name.innerHTML = strAttention('Отсутствует значение константы диссоциации - выберите соединение');
	}
	var c =  document.getElementById('subc');	
	if (parseFloat(c.innerText) <= 0) {
		errors++;
		name.innerHTML = strAttention('Некорректное значение концентрации');
	}
	/* - - - - - - - -  - - - - - */
	// Check radio - mode block
	var m1 = document.getElementById('tradioM1').checked; 
	var m2 = document.getElementById('tradioM2').checked;
	if ((m1 != true) && (m2 != true)) {
		errors++;
		alert('Не выбран режим калькулятора - частная или общая формула');

		if (0 == errors) {
			calcph();
		}
	}
}

function pcc_calcpH(name, c, m, kw, om, mi, uo, k1, k2, k3, k4)	{
	if ((typeof name != 'undefined') ||
		(typeof c != 'undefined') ||
		(typeof m != 'undefined')) {
			return (-1);
	}
	
	xhr.open("POST", "http://chem-space.appspot.com/ph-calc-api", true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
	var param = 'CHEM_API_ACTION=' + encodeURIComponent('CHEM_API_PH') + 
				'&CHEM_API_PH_FORMULA=' + encodeURIComponent(name) + 
				'&CHEM_API_PH_CONCENTRATION=' + encodeURIComponent(c) +  '&CHEM_API_PH_MODE=' + encodeURIComponent(m);
				
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			if(xhr.status == 200) {
			var res = document.getElementById('resultPH');
			res.innerHTML = xhr.responseText;							
			}
		}
	};
	
	if (typeof kw != 'undefined') {
		param += '&CHEM_API_PH_KW' + encodeURIComponent(kw);
	}
	if (typeof om != 'undefined') {
		param += '&CHEM_API_PH_OUTPUTMODE' + encodeURIComponent(om);
	}
	if (typeof mi != 'undefined') {
		param += '&CHEM_API_PH_MAXITER' + encodeURIComponent(mi);
	}
	if (typeof uo != 'undefined') {
		if ((1 == uo) && (typeof k1 != 'undefined')) {
			param += '&CHEM_API_PH_USEOWN' + encodeURIComponent(uo);
			param += '&CHEM_API_PH_KD1' + encodeURIComponent(k1);
			if (typeof k2 != 'undefined') {
				param += '&CHEM_API_PH_KD2' + encodeURIComponent(k2);
			}
			if (typeof k3 != 'undefined') {
				param += '&CHEM_API_PH_KD3' + encodeURIComponent(k3);			
			}
		}
	}
	xhr.send(param);
}

/* скопировано из v1 / написано 05-06-2015 */

function pcc_html_clearAllPreviewData() {
	// var pd = document.getElementById('peviewData');
	document.getElementById('subname').innerText = '';
	document.getElementById('subf').innerText = '';
	document.getElementById('subKd').innerText = '';
	document.getElementById('subc').innerText = '';	
	document.getElementById('tradioM1').checked = true;	

	// setConcentration();
}

function pcc_html_isAbleAutoSelectSubstace() {
	var table = document.getElementById('substancesTable');
	if (null != table) {
		var lastRowsN = table.getElementsByClassName('chemapi_abkdLRow').length;
		var rowsN = table.rows.length;
		if (rowsN - lastRowsN = 2) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return (-1);
	}
	return (-1);
}

function pcc_html_formatFormulas(rootElem) {
	// <div class="f"></div>
	var obj = document.getElementById(rootElem);
	var arr = obj.getElementsByClassName('f');
	// :TODO: Check if all work fine, cause auto-length can differ from amount of elements
	for (var i = 0; i < arr.length; i++) {					
		arr[i].innerHTML = arr[i].innerHTML.replace(/([\[\(]?)([[A-z]+)([\d]*)([\]\)]?)([\d]*)/g, '$1$2<sub>$3</sub>$4<sub>$5</sub>');
	}
}

function pcc_seachSubstance() {				
	var str = document.getElementById('subf').value;
	if (str.length < 2) {
		alert('Минимальная строка поиска - 2 символа');
		return (-1);
	} else {					
		xhr.open("POST", "http://localhost/cms/workspace/etc/ph/chemCalc_API.php", true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
		//xhr.setRequestHeader('CHEM_API_ACTION', 'CHEM_API_SEARCHABKD');xhr.setRequestHeader('CHEM_API_SEARCHABKD_STRING', str);
		xhr.onreadystatechange = function() {
		  if (xhr.readyState == 4) {
			if(xhr.status == 200) {
				var table = document.getElementById('substancesTable');//.children[0];
				table.innerHTML = xhr.responseText;
				formatFormulas('substancesTable');
				if (true == isAbleAutoSelectSubstace()) {
					prepareCalc();
				}
			} else {								
				alert('Произошла ошибка. Строка результата: ' + ('' != xhr.responseText ? xhr.responseText : ' отсутствует.'));				
				return (-1);
			}
		  }
		};				
		var div_str = '<div class="selectSubstanceTD" width="100%" height="100%" onclick="selectSubstance(this)">+</div>';
		xhr.send('CHEM_API_ACTION=' + encodeURIComponent('CHEM_API_SEARCHABKD') + 
				 '&CHEM_API_SEARCHABKD_STRING=' + encodeURIComponent(str) + 
				 '&CHEM_API_SEARCHABKD_MAXN=' + encodeURIComponent(15) + 
				 '&CHEM_API_SEARCHABKD_CASESEN=' + encodeURIComponent(0) + 
				 '&CHEM_API_SEARCHABKD_STYLE=' + encodeURIComponent(5) + 
				 '&CHEM_API_SEARCHABKD_TDITEM_TEXT=' + encodeURIComponent(div_str) + 							 
				 '&CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT=' + encodeURIComponent('Выбрать вещество') + 
				 '&CHEM_API_SEARCHABKD_TRITEM_ATTR=') + encodeURIComponent('') + 
				 '&CHEM_API_SEARCHABKD_TRLASTTEXT=' +  + encodeURIComponent('');	
	}
	return 0;
}


window.onload = function() {
    var xhr = new XMLHttpRequest();
    pcc_html_clearAllPreviewData();    
    
    document.getElementById("btnCalc").addEventListener("click", pcc_calcpH, false);
    document.getElementById("btnSearch").addEventListener("click", pcc_seachSubstance, false);    
};

