/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov 
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/

function seachSubstance() {				
	var str = document.getElementById('teditSubstance').value;
	if (str.length < 2) {
		alert('Минимальная строка поиска - 2 символа');
		return (-1);
	} else {					
		xhr.open("POST", "http://localhost/cms/workspace/etc/ph/chemCalc_API.php", true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
		//xhr.setRequestHeader('CHEM_API_ACTION', 'CHEM_API_SEARCHABKD');xhr.setRequestHeader('CHEM_API_SEARCHABKD_STRING', str);
		xhr.onreadystatechange = function() {
		  if (xhr.readyState == 4) {
			if(xhr.status == 200) {
				var table = document.getElementById('substancesTable');//.children[0];
				table.innerHTML = xhr.responseText;
				formatFormulas('substancesTable');
				if (true == isAbleAutoSelectSubstace()) {
					prepareCalc();
				}
			} else {								
				alert('Произошла ошибка. Строка результата: ' + ('' != xhr.responseText ? xhr.responseText : ' отсутствует.'));				
				return (-1);
			}
		  }
		};				
		var div_str = '<div class="selectSubstanceTD" width="100%" height="100%" onclick="selectSubstance(this)">+</div>';
		xhr.send('CHEM_API_ACTION=' + encodeURIComponent('CHEM_API_SEARCHABKD') + 
				 '&CHEM_API_SEARCHABKD_STRING=' + encodeURIComponent(str) + 
				 '&CHEM_API_SEARCHABKD_MAXN=' + encodeURIComponent(15) + 
				 '&CHEM_API_SEARCHABKD_CASESEN=' + encodeURIComponent(0) + 
				 '&CHEM_API_SEARCHABKD_STYLE=' + encodeURIComponent(5) + 
				 '&CHEM_API_SEARCHABKD_TDITEM_TEXT=' + encodeURIComponent(div_str) + 							 
				 '&CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT=' + encodeURIComponent('Выбрать вещество') + 
				 '&CHEM_API_SEARCHABKD_TRITEM_ATTR=') + encodeURIComponent('') + 
				 '&CHEM_API_SEARCHABKD_TRLASTTEXT=' +  + encodeURIComponent('');	
	}
	return 0;
}

function calcPH() {
	var _ss = document.getElementById('peviewData');
	var m1 = document.getElementById('tradioM1').checked; 
	var m2 = document.getElementById('tradioM2').checked;
	
	if ((_ss.children[1].innerText != '') && (_ss.children[2].innerHTML != '') && (_ss.children[3].innerHTML != '') && (parseFloat(_ss.children[6].innerHTML) > 0) && (m1 != m2)) {					
		xhr.open("POST", "http://localhost/cms/workspace/etc/ph/chemCalc_API.php", true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');					
		
		xhr.onreadystatechange = function() {
		  if (xhr.readyState == 4) {
			if(xhr.status == 200) {
				var res = document.getElementById('resultPH');
				res.innerHTML = xhr.responseText;							
			}
		  }
		};		
		
		xhr.send('CHEM_API_ACTION=' + encodeURIComponent('CHEM_API_PH') + 
				 '&CHEM_API_PH_FORMULA=' + encodeURIComponent(_ss.children[1].innerText) + 
				 '&CHEM_API_PH_CONCENTRATION=' + encodeURIComponent(_ss.children[6].innerHTML) + 							 
				 '&CHEM_API_PH_MODE=' + encodeURIComponent(true == m1 ? 0 : 1) + 
				 '&CHEM_API_PH_OUTPUTMODE=' + encodeURIComponent(0) + 
				 '&CHEM_API_PH_KW=' + encodeURIComponent('1E-13')
				 );
	} else {
		return (-1);
	}
	return 0;
}
function prepareCalc() {
	var ss = document.getElementById('peviewData');
	var m1 = document.getElementById('tradioM1').checked; 
	var m2 = document.getElementById('tradioM2').checked;
	
	if ((ss.children[1].innerText != '') && (ss.children[2].innerHTML != '') && (ss.children[3].innerHTML != '') && (parseFloat(ss.children[6].innerHTML) > 0) && (m1 != m2)) {
		if (0 != calcPH()) {
			return (-1);
		}
	} else {
		setConcentration();
		if (0 != seachSubstance()) {
			if (false == isPreValidPreviewData()) {
				clearPreviewData();
			}		
		}
	}
}

function formatFormulas(rootElem) {
	// <div class="f"></div>
	var obj = document.getElementById(rootElem);
	var arr = obj.getElementsByClassName('f');
	// :TODO: Check if all work fine, cause auto-length can differ from amount of elements
	for (var i = 0; i < arr.length; i++) {					
		arr[i].innerHTML = arr[i].innerHTML.replace(/([\[\(]?)([[A-z]+)([\d]*)([\]\)]?)([\d]*)/g, '$1$2<sub>$3</sub>$4<sub>$5</sub>');
	}
}

function selectSubstance(obj) {
	if (obj.parentNode.className == 'chemapi_abkdCol1TD') {
		// div < td < tr < tbody < table
		var _tr = obj.parentNode.parentNode;
		var _ss = document.getElementById('peviewData');
		_ss.children[1].innerHTML = _tr.children[1].innerHTML;
		_ss.children[2].innerHTML = _tr.children[2].innerHTML;
		_ss.children[3].innerHTML = '<br/>1 (' + _tr.children[4].innerHTML + ') <br/>2 (' + _tr.children[5].innerHTML + ')<br/>3 (' + _tr.children[6].innerHTML + ') <br/>4 (' + _tr.children[7].innerHTML + ')';
		_ss.children[6].innerHTML = document.getElementById('teditConcentr').value;
	}
}

function isPreValidPreviewData() {
	var _ss = document.getElementById('peviewData');
	return ((_ss.children[1].innerText != '') && (_ss.children[2].innerHTML != '') && (_ss.children[3].innerHTML != ''));
}

function clearPreviewData() {
	var _ss = document.getElementById('peviewData');
	_ss.children[1].innerHTML = 'не выбрано';
	_ss.children[2].innerHTML = '';
	_ss.children[3].innerHTML = '';	
	setConcentration();
}

function clearAllPreviewData() {
	clearPreviewData();
	setConcentration(-1);
}

function setConcentration() {
	var _ss = document.getElementById('peviewData');
	var c = document.getElementById('teditConcentr').value;
	// c = ((с < 0) ? (document.getElementById('teditConcentr').value) : c);	
	_ss.children[6].innerHTML = parseFloat(c) > 0 ? c : '<font color="red"> концентрация введена неверно, </font>' + escape(c) + ' недопустимое значение.';
}

function isAbleAutoSelectSubstace() {
	var table = document.getElementById('substancesTable');
	if (null != table) {
		var lastRowsN = table.getElementsByClassName('chemapi_abkdLRow').length;
		var rowsN = table.rows.length;
		if (rowsN - lastRowsN = 2) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return (-1);
	}
	return (-1);
}

var xhr = new XMLHttpRequest();
document.getElementById('tradioM1').checked = true;
clearAllPreviewData();