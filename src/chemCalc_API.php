<?php

/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/

/**
 * ChemModule: Chemical API
 * 
 * @author optimLight
 * @copyright 2013
 */


/**
 * :TODO:
 * -   use global variables such as $AcidBaseKdTable ...
 * -    when read data from file - convert all numbers into corresponding types
 * -    safe string  $str_tr_item, $str_start_item, $str_start_cell, $str_start_end and all other strings from POST
 * -    show time and RAM usage 
 */

// for including chemCalc_*.php scripts
define('CHEMMODULEAPI', 'Chem API defined');

require_once ('chemCalc_pH.php');
require_once ('chemCalc_RU.php');
require_once ('chemCalc_DB.php');
require_once ('chemCalc.inc.php');



function chem_pref_checkDomain() {
    global $_CHEM_CALC_RT_DATA, $_CHEM_CALC_SETTINGS;  
    
    $sHost = array_key_exists( pureUrlToDomain( $_SERVER['HTTP_HOST'] ), $_CHEM_CALC_SETTINGS[CFG_ALLOWED_DOMAINS] ) ? pureUrlToDomain( $_SERVER['HTTP_HOST'] ) : FALSE;
    $sReferer = array_key_exists( pureUrlToDomain( $_SERVER['HTTP_REFERER'] ), $_CHEM_CALC_SETTINGS[CFG_ALLOWED_DOMAINS] ) ? pureUrlToDomain( $_SERVER['HTTP_REFERER'] ) : FALSE;
    
    if ( ( $sHost != FALSE ) && ( $sReferer != FALSE ) ) {
        $_CHEM_CALC_RT_DATA[CFG_ALLOWED_CALC] = TRUE;      
    } else {
        $_CHEM_CALC_RT_DATA[CFG_ALLOWED_CALC] = FALSE;
    }
    
    return $_CHEM_CALC_RT_DATA[CFG_ALLOWED_CALC];
}


function chem_pref_checkHTTPMode(&$str_log) {
    global $_CHEM_CALC_RT_DATA, $_CHEM_CALC_SETTINGS; 
    
    $http_mode = isset( $_GET[CHEM_API_MODE] ) ? $_GET[CHEM_API_MODE] : ( isset( $_POST[CHEM_API_MODE] ) ? $_POST[CHEM_API_MODE] : '' );
    if ('' == $http_mode)
    {
        $_CHEM_CALC_RT_DATA[CHEM_API_MODE] = '';
        $str_log .= '. Function stops. HTTP mode is not specified';
    }
    
    if ( ( CHEM_API_MODE_POST == $http_mode ) && ( $_CHEM_CALC_SETTINGS[CFG_ALLOWED_DOMAINS][pureUrlToDomain( $_SERVER['HTTP_HOST'])][CFG_DOMAIN_ALLOWED_POST] ) ) {
        $_CHEM_CALC_RT_DATA[CHEM_API_MODE] = CHEM_API_MODE_POST;
    } elseif ( ( CHEM_API_MODE_GET == $http_mode ) && ( $_CHEM_CALC_SETTINGS[CFG_ALLOWED_DOMAINS][pureUrlToDomain( $_SERVER['HTTP_HOST'])][CFG_DOMAIN_ALLOWED_GET] ) ) {
        $_CHEM_CALC_RT_DATA[CHEM_API_MODE] = CHEM_API_MODE_GET;
    } else {
        $str_log .= '. Current mode is not allowed for this domain';
    }
    
    return $_CHEM_CALC_RT_DATA[CHEM_API_MODE];
}

function chem_pref_checkAction(&$str_log) {
    global $_CHEM_CALC_RT_DATA, $_CHEM_CALC_SETTINGS; 
    
    switch ( $_CHEM_CALC_RT_DATA[CHEM_API_MODE] ) {
        case CHEM_API_MODE_GET:
            $_CHEM_CALC_RT_DATA[CHEM_API_ACTION] = isset($_GET[CHEM_API_ACTION]) ? $_GET[CHEM_API_ACTION] : '';
            break;
        case CHEM_API_MODE_POST:
            $_CHEM_CALC_RT_DATA[CHEM_API_ACTION] = isset($_POST[CHEM_API_ACTION]) ? $_POST[CHEM_API_ACTION] : '';
            break;
    }
    
    return $_CHEM_CALC_RT_DATA[CHEM_API_ACTION];
}

function chem_out_pH($om = 0) {
    global $_CHEM_CALC_RT_DATA;
    switch ($om)
    {
        case 0:
            {
                echo '[H<sub>3</sub>O<sup>+</sup>] = ' . $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][13] .
                     ', pH = ' . $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][11] .
                     ' | [OH<sup>-</sup>] = ' . $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][14] .
                     ', pOH = ' . $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][12];
            }
        // no echo output
        case 2:
            {
                
            }
    }
}

/**
 * General API used to calculate concentration of \f$H_{3}O^{+}\f$ and \f$OH^{-}\f$ ions. Direct access not recommended
 * 
 * @param mixed $c concentration
 * @param mixed $m choose equation, by which "pH" will be calculated
 * @param mixed $f formula or name that uniquely identifies substance
 * @param mixed $log output string for additional information, should be empty
 * @param integer $Kw constant - autoprotolysis
 * @param integer $om short from "OutputMode" - Ajax (1) or HTML (0) retuned data type
 * @param integer $mi number of maimum iterations (if required by equation)
 * @param integer $e accuracy (if required by equation)
 * @param integer $cs start from this concentration (if required by equation) 
 * @param integer $ce maximum concentration (if required by equation)
 * @return type depends on $om param
 */
function chem_pH($c, $m, $f, $Kw = 1.0E-14, $om = 0, $mi = 150, $ou = 0, $Kd1 = 0.0, $Kd2 = 0.0, $Kd3 = 0.0, $e =
    1.0E-15, $cs = 1.0E-24, $ce = 1.0E+03)
{
    // if don't use own Kd or setted $Kd is <= 0
    if ((0 == $ou) || (0 >= $Kd1))
    {
        global $chemDB_tables, $_CHEM_CALC_RT_DATA;
        $table = chemDB_readAcidBaseKdTable($chemDB_tables[CHEM_API_PH][CHEM_API_ARRAY_TABLE]);
        $data = chemDB_findAcidBaseKdValue($f, $table);
        if ($data != (-1))
        {
            $Kd1 = (float)$data[3]; // !!! or use &$Kd1 ...
            $Kd2 = (float)$data[4];
            $Kd3 = (float)$data[5];
            
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][2] = $data[0]; // name
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][1] = $data[1]; // formula
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][8] = $data[2]; // temperature
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][4] = (float)$data[3]; // Kd1
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][5] = (float)$data[4]; // Kd2
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][6] = (float)$data[5]; // Kd3
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][7] = (float)$data[6]; // Kd4
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][9] = (integer)$data[7]; // literature
            $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][10] = (float)$c; // molar concentration
        } else
            return (-1);
    }
    
    // otherwise use send $Kd values
    $log = '';
    $result = 0 == $m ? chemCalc_concentrationHOH_commonF($c, $Kd1, $Kd2, $Kd3, $log, $Kw, $e, $mi, $cs, $ce) :
        chemCalc_concentrationHOH_simplifiedF($c, $Kd1, $Kw);
        
    if ( (-1) != $result ) {
        $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][11] = (float)((-1) * log($result, 10)); // pH
        $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][12] = (float)((-1) * log(($Kw / $result), 10)); // pOH
        $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][13] = (float)($result); // [H]
        $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][14] = (float)($Kw / $result); // [OH]
        
        chem_out_pH($om);
    }
    
    return (-1) != $result ? 0 : (-1);
}

/**
 * Format of Kd and pKd output
 * 
 * @param mixed $Kd dissociation constant
 * @param integer $s style (0-5)
 * @param string $if N if $Kd == 0 this string will be used, otherwise $Kd will be used
 * @param integer $nDigits maximum number of digits after comma :TODO:
 * @return string
 */
function _pKdfmt($Kd, $s = 0, $ifN = '-', $decimals = 2, $decpoint = '.')
{
    $K = (float)$Kd;
    if (('' != $ifN) && (0 == $Kd))
    {
        return $ifN;
    }
    switch ($s)
    {
        case 0:
            {
                return number_format(((-1) * log($K, 10)), $decimals, $decpoint);
            }
        case 1:
            {
                return 'pK = ' . number_format((-1) * log($K, 10), $decimals, $decpoint);
            }
        case 2:
            {
                return 'pK = ' . number_format((-1) * log($K, 10), $decimals, $decpoint) . ' | K = ' . number_format($K, $decimals, $decpoint);
            }
        case 3:
            {
                return number_format($K, $decimals, $decpoint);
            }
        case 4:
            {
                return 'K = ' . number_format($K, $decimals, $decpoint);
            }
        case 5:
            {
				return sprintf('K = %.' . (string)$decimals  . 'e' . ' | pK = %01.' . (string)$decimals . 'f', $K, ((-1) * log($K, 10)));
            }
    }
}

/**
 * Returns string with formated data from database about substance which were identified by send $param and other settings 
 * 
 * @param string $string full or part of namem formula or other data in string format where $param appears 
 * @param integer $N maximum number of retuned items in array
 * @param bool $reg use or not case-sensetive search. ?use if $RegExp == TRUE? :TODO: 
 * @param bool $regexp use RegExp :TODO:
 * @param integer $om short from "OutputMode" - Ajax (1) or HTML (0) retuned data type
 * @param integer $style specified style of retuned string
 * @param string $str_tr string in "html format" for attributes. This string will be added into each line in the retuned table   
 * @param string $str_item string on each line in the first colomn (additional param)
 * @param string $str_1col string in the first colomn (extra colomn will be added)
 * @param string $str_lastRow extra last row in the retuned table
 * @return string
 */
function chem_searchAcidBaseKd($string, $N, $reg, $regexp, $om, $style, $str_tr = '', $str_item = '', $str_1col = '', $str_lastRow =
    '')
{
    if ((strlen($string) < 2) || (strlen($string) > 40))
    {
        return (-1);
    }
    global $chemDB_tables;
    $table = chemDB_readAcidBaseKdTable($chemDB_tables[CHEM_API_PH][CHEM_API_ARRAY_TABLE]);
    $amount = 0;
    $result = chemDB_searchNAcidBaseKdValue($string, $table, $N, $amount, $reg, $regexp);
    if (-1 == $result)
    {
        return (-1);
    }
    $str = '';
    if ($amount > 0)
    {
        switch ($om)
        {
            case 0:
                {
                    // $str_item instead of $str_start_cell was used here for easy viewing
                    // :TODO: add <td>Notes</td>
					// :WARNING: Use class only, not id for tags
					$str = '<table class="chemapi_abkdTable table"><tr class="chemapi_abkdDesc">' . 
							('' != $str_item ? '<td class="chemapi_abkdCol1">' . $str_1col . '</td>' : '') .
						'<td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_NAME]	. 
						'</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_FORMULA] .
						'</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_TEMP] . ', <sup>0</sup>C' .
						'</td><td>Kd1</td>' .
						'<td>Kd2</td>' .
						'<td>Kd3</td>' .
						'<td>Kd4</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_SOURCE] .
						'</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_NOTES] .
						'</td></tr>';
                    $i = 0;
                    for ($i = 0; $i < $amount; $i++)
                    {
						$str .= '<tr' . $str_tr . '>' . ('' != $str_item ? '<td class="chemapi_abkdCol1TD">' . $str_item . '</td>' : '') . // start colomn
                            '<td>' . $result[$i][0] . // name
							'</td><td><span class="f">' . $result[$i][1] . // fromula
							'</span></td><td>' . $result[$i][2] . // temperature
                            '</td><td>' . _pKdfmt($result[$i][3], $style) . // Kd1
                            '</td><td>' . _pKdfmt($result[$i][4], $style) . // Kd2
                            '</td><td>' . _pKdfmt($result[$i][5], $style) . // Kd3
                            '</td><td>' . _pKdfmt($result[$i][6], $style) . // Kd4
                            '</td><td>' . $result[$i][7] . // Source
                            //'</td><td>' . $result[$i][8] .                                                // Notes
                        '</td></tr>';
                    }
					$str .= ('' != $str_lastRow ? '<tr class="chemapi_abkdLRow"><td>' . $str_lastRow . '<span class="chemapi_abkdLog"' . sprintf('%d $s %d', $amount, $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_RESULTS], $N) . '</span></td></tr>' : '') . '</table>';
                    echo $str;
                    return 0;
                }
            case 10:
                {
                    $_QURL = $_GET;
                    array_shift($_QURL);
                    // $str_item instead of $str_start_cell was used here for easy viewing
                    // :TODO: add <td>Notes</td>
					// :WARNING: Use class only, not id for tags
					$str = '<table class="chemapi_abkdTable table table-hover table-condensed table-striped"><thead><tr class="chemapi_abkdDesc">' . 
							('' != $str_item ? '<td class="chemapi_abkdCol1">' . $str_1col . '</td>' : '') .
                        '<td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_NAME]	. 
						'</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_FORMULA] .
						'</td><td>' .
						$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_TEMP] . ', <sup>0</sup>C' .
						'</td><td>Kd1</td>' .
						'<td>Kd2</td>' .
						'<td>Kd3</td>' .
						'<td>Kd4</td></tr></thead>';
                    $i = 0;
                    for ($i = 0; $i < $amount; $i++)
                    {
						$str .= '<tbody><tr' . $str_tr . '>' . ('' != $str_item ? '<td class="chemapi_abkdCol1TD">' . $str_item . '</td>' : '') . // start colomn
                            '<td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], $result[$i][0] ) . // name
							'</td><td><span class="f">' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], $result[$i][1] ) . // fromula
							'</span></td><td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], $result[$i][2] ) .  // temperature
                            '</td><td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], _pKdfmt($result[$i][3], $style) ) . // Kd1
                            '</td><td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], _pKdfmt($result[$i][4], $style) ) . // Kd2
                            '</td><td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], _pKdfmt($result[$i][5], $style) ) . // Kd3
                            '</td><td>' . genSpecURL($_QURL, CFG_SELF_LINK, CHEM_API_PH_FORMULA, $result[$i][1], _pKdfmt($result[$i][6], $style) ) . // Kd4                            
                            '</td></tr></tbody>';
                    }
					$str .= ('' != $str_lastRow ? '<tr class="chemapi_abkdLRow"><td>' . $str_lastRow . '<span class="chemapi_abkdLog"' . sprintf('%d $s %d', $amount, $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_RESULTS], $N) . '</span></td></tr>' : '') . '</table>';
                    echo $str;
                    return 0;
                }
        }
    }
    return (-1);
}

function chem_main_ph(&$str_log = null)
{
    global $_CHEM_CALC_RT_DATA, $_CHEM_CALC_SETTINGS;  
    
    $_CHEM_ARR = array();
    
    switch ( $_CHEM_CALC_RT_DATA[CHEM_API_MODE] ) {
        case CHEM_API_MODE_GET:
            $_CHEM_ARR = $_GET;
            break;
        case CHEM_API_MODE_POST:
            $_CHEM_ARR = $_POST;
            break;
    }
    
    $c = isset($_CHEM_ARR[CHEM_API_PH_CONCENTRATION]) ? $_CHEM_ARR[CHEM_API_PH_CONCENTRATION] : 0;
    $f = isset($_CHEM_ARR[CHEM_API_PH_FORMULA]) ? $_CHEM_ARR[CHEM_API_PH_FORMULA] : '';
    //$c = ((FALSE == is_float($c)) && (FALSE == is_int($c))) ? 0 : $c;
    // $f = htmlspecialchars($_CHEM_ARR[CHEM_API_PH_FORMULA]);

    $Kw = isset($_CHEM_ARR[CHEM_API_PH_KW]) ? $_CHEM_ARR[CHEM_API_PH_KW] : 1E-14;	
    $m = isset($_CHEM_ARR[CHEM_API_PH_MODE]) ? $_CHEM_ARR[CHEM_API_PH_MODE] : 0;
    $om = isset($_CHEM_ARR[CHEM_API_PH_OUTPUTMODE]) ? $_CHEM_ARR[CHEM_API_PH_OUTPUTMODE] : 0;
    $mi = isset($_CHEM_ARR[CHEM_API_PH_MAXITER]) ? $_CHEM_ARR[CHEM_API_PH_MAXITER] : 150;
    $uo = isset($_CHEM_ARR[CHEM_API_PH_USEOWN]) ? $_CHEM_ARR[CHEM_API_PH_USEOWN] : 0;
    if (0 != $uo)
    {
        $Kd1 = $_CHEM_ARR[CHEM_API_PH_KD1];
        $Kd2 = $_CHEM_ARR[CHEM_API_PH_KD2];
        $Kd3 = $_CHEM_ARR[CHEM_API_PH_KD3];
        $Kd1 = (false == is_numeric($Kd1)) ? 0 : $Kd1 + 0;
		if (0 == $Kd1) {
			$str_log .= ' Not valid value for Kd1.';
			return (-1);
		}
    }
    
    if ((0 == $c) || ( ( strlen($f) < 2 ) && ( 0 == $uo ) ) )
    {
        if (null != $str_log)
        {
			$str_log .= ' Error occurred - not specified concentration or formula.';
        } else
        {
            // $exit_str . ' Error occurred - not specified $c or $f.';
        }
        return (-1);
    }
    
    // $E, $CS and $CE can be default
    return (0 == $uo) ? chem_pH($c, $m, $f, $Kw, $om, $mi) : chem_pH($c, $m, $f, $Kw, $om, $mi, $uo, $Kd1, $Kd2, $Kd3);
}

function chem_main_searchABKd(&$str_log = null)
{
    global $_CHEM_CALC_RT_DATA, $_CHEM_CALC_SETTINGS;  
    
    $_CHEM_ARR = array();
    
    switch ( $_CHEM_CALC_RT_DATA[CHEM_API_MODE] ) {
        case CHEM_API_MODE_GET:
            $_CHEM_ARR = $_GET;
            break;
        case CHEM_API_MODE_POST:
            $_CHEM_ARR = $_POST;
            break;
    }
    
    $str = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_STRING]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_STRING] : '';
    $n = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_MAXN]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_MAXN] : 10;
    $r = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_CASESEN]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_CASESEN] : FALSE;
    $re = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_REGEXP]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_REGEXP] : false;
    $om = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_OUTPUTMODE]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_OUTPUTMODE] : 0;
    $s = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_STYLE]) ? $_CHEM_ARR[CHEM_API_SEARCHABKD_STYLE] : 5;
    $str_it = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_TDITEM_TEXT]) ? stripcslashes($_CHEM_ARR[CHEM_API_SEARCHABKD_TDITEM_TEXT]) : '';
    $str_tr = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_TRITEM_ATTR]) ? stripcslashes($_CHEM_ARR[CHEM_API_SEARCHABKD_TRITEM_ATTR]) : '';
    $str_c = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT]) ? stripcslashes($_CHEM_ARR[CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT]) : '';
    $str_e = isset($_CHEM_ARR[CHEM_API_SEARCHABKD_TRLASTTEXT]) ? stripcslashes($_CHEM_ARR[CHEM_API_SEARCHABKD_TRLASTTEXT]) : '';

    if ((strlen($str) < 2) || (strlen($str) > 40))
    {
        $str_log .= '. Length of searched string must be 2-40 symbols';
        return (-1);
    }
    return chem_searchAcidBaseKd($str, $n, $r, $re, $om, $s, $str_tr, $str_it, $str_c, $str_e);
    // return chem_searchAcidBaseKd($str, 10, false, false, 0, 5);
}

function chem_main()
{
    global $_CHEM_CALC_RT_DATA;
    
    $str_log = 'Function starts';
    
    if ( !chem_pref_checkDomain() ) {
        $str_log .= '. Function stops. This is not valid domain';
        return (-1);
    }
    
    $http_mode = chem_pref_checkHTTPMode($str_log);
    if ( '' == $http_mode ) {
        $str_log .= '. Function stops.';
        return (-1);
    }
    
    $action = chem_pref_checkAction($str_log);
    if ('' == $action)
    {
        echo $str_log . '. Function stops. Access denied.';
        return (-1);
    }
    
    switch ($action)
    {
        case CHEM_API_PH:
            {
                $str_log .= ' > CHEM_API_PH';
                if ( (-1) != chem_main_ph($str_log) ) {
                    $str_log .= ' : Result were returned. No errors occurred.';
                    $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][0] = 1;
                } else {
                    $str_log .= ' : Error occurred. Bad result data';   
                    $_CHEM_CALC_RT_DATA[CHEM_API_OUTPUT_PH_DATA][0] = -1;
                } 
                break;
            }

        case CHEM_API_SEARCHABKD:
            {
                $str_log .= ' > CHEM_API_SEARCHABKD';
                (-1) != chem_main_searchABKd($str_log) ? $str_log .= ' : Result were returned. No errors occurred.' : $str_log .=
                    ' : Error occurred. Bad result data';
                break;
            }
    }
	return '';
	// $str_log . '. Function ends.';
}

$start = microtime(true);

                $_SERVER['HTTP_HOST'] = 'chem-space.pp.ua';
                $_SERVER['HTTP_REFERER'] = 'chem-space.pp.ua';

chem_main();
$time = microtime(true) - $start;
// echo '<br/> Time usage: ' . $time;

?>