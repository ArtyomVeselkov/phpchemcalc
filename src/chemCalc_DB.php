<?php

if (!defined('CHEMMODULEAPI'))
    exit('No direct script access allowed');

/******************************************************************************
 * Copyright (c) 2013, Artyom Veselkov
 *  
 * This file is part of the phpChemCalc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 *
 *   The above copyright notice and this permission notice shall be included in 
 *   all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * ( Copyright (c) 2013, Артём Веселков
 *
 *   Этот файл — часть phpChemCalc
 *
 *   Данная лицензия разрешает лицам, получившим копию данного программного
 *   обеспечения и сопутствующей документации (в дальнейшем именуемыми 
 *   «Программное Обеспечение»), безвозмездно использовать Программное 
 *   Обеспечение без ограничений, включая неограниченное право на 
 *   использование, копирование, изменение, добавление, публикацию, 
 *   распространение, сублицензирование и/или продажу копий Программного
 *   Обеспечения, также как и лицам, которым предоставляется данное Программное
 *   Обеспечение, при соблюдении следующих условий: 
 *
 *     Указанное выше уведомление об авторском праве и данные условия должны
 *     быть включены во все копии или значимые части данного Программного
 *     Обеспечения.
 *
 *   ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 *   ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ 
 *   ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО
 *   КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ
 *   АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО ИСКАМ О ВОЗМЕЩЕНИИ
 *   УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ
 *   ИЛИ ИНОМУ, ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ
 *   ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ 
 *   ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.                                   )
 *****************************************************************************/	

/**
 * ChemModule: Database funtions
 * 
 * @author optimLight
 * @copyright 2013
 */

/**
 * Read CSV file into array[#][#]. Values are string. Use UTF encoding
 * 
 * @param mixed $csvFile path to csv file
 * @return array nested array array[#][#]
 */
function readCSV($csvFile)
{
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) && (is_resource($file_handle)))
    {
        $line_of_text[] = fgetcsv($file_handle, 1024, ';');
    }
    fclose($file_handle);
    return $line_of_text;
}

/**
 * Search in "double" nested array a value with array_search function
 * 
 * @param mixed $array array to search in
 * @param mixed $value searched value 
 * @param bool $strict uses in array_search function
 * @param string $sep currently not used (for array_search function)
 * @return integer index of found element in array [index][#]. If not found - returns -1
 */
function array2_search($array, $value, $strict = false, $sep = ';')
{
    foreach ($array as $index => $subarr)
    {
        $h_index = array_search($value, $subarr, $strict);
        if (!is_bool($h_index))
        {
            return $index;
        }
    }

    return (-1);
}

/**
 * Read data table with dissociation constants for acids and bases
 * 
 * @param string $file_path leave empty for default path or use own path
 * @return mested "double" array
 */
function chemDB_readAcidBaseKdTable($file_path = 'test.csv')
{
	if  ( TRUE == CFG_USE_AUTOPATH ) {
		return readCSV(CFG_PCC_FULLPATH . CFG_STATIC_RELPATH . '/' . $file_path);
    } else {
		return readCSV($file_path);
    }
}

/**
 * Read data table with dissociation constants for bases only
 * 
 * @param string $file_path leave empty for default path or use own path
 * @return mested "double" array
 */
function chemDB_readBaseKdTable($file_path = 'test.csv')
{
	if  ( TRUE == CFG_USE_AUTOPATH ) 
		return readCSV(CFG_PCC_FULLPATH . CFG_STATIC_RELPATH . '/' . $file_path);
	else
		return readCSV($file_path);
}

/**
 * Read data table with dissociation constants for acids only
 * 
 * @param string $file_path leave empty for default path or use own path
 * @return mested "double" array
 */
function chemDB_readAcidKdTable($file_path = 'test.csv')
{
	if  ( TRUE == CFG_USE_AUTOPATH )
		return readCSV(CFG_PCC_FULLPATH . CFG_STATIC_RELPATH . '/' . $file_path);
	else
		return readCSV($file_path);
}

/**
 * Use default search function to get array with data about dissociation for needed substance
 * 
 * @param mixed $name Brutto formula or name that will identify needed substance
 * @param mixed $table array is gotten by funtion chemDB_readAcidBaseKdTable
 * @return array with data for specified substance or (-1) if data wasn't find
 */
function chemDB_findAcidBaseKdValue($name, $table)
{
    $index = array2_search($table, $name);
    return $index != (-1) ? $table[$index] : (-1);
}

/**
 * Get array of suitable data of dissociation constants for specified name or formula
 * 
 * @param mixed $param name or formula (or part of them) to search
 * @param mixed $table where search
 * @param mixed $N maximum number of returned items in array
 * @param mixed $count should be 0 - returns the actual amount of items in returned array
 * @param bool $reg case-insensitive :TODO:
 * @param bool $regexp use RegExp ($param should be an RegExp compatible string) :TODO:
 * @return array of found items
 */
function chemDB_searchNAcidBaseKdValue($param, $table, &$N, &$count, $reg = false, $regexp = false)
{
    $N = (integer)$N > 20 ? 20 : $N;
    $N = (integer)$N < 2 ? 2 : $N;
    $count = 0;
    $found = array();    
	try
    {
		if (FALSE == $reg) 
		{
			foreach ($table as $index1 => $subarr)
			{
				// :TODO: check if not nested array
				if (is_array($subarr))
				{
					foreach ($subarr as $index2 => $item)
					{
						if (('string' == gettype($item)) && (false !== mb_stripos($item, $param, 0, "utf-8")) && ($count < (integer)$N))
						{
							array_push($found, $subarr);
							$count++;
						} elseif ($count > $N)
						{
							return $found;
							break;
						}
					}
				}
			}
		} else {
			foreach ($table as $index1 => $subarr)
			{
				// :TODO: check if not nested array
				if (is_array($subarr))
				{
					foreach ($subarr as $index2 => $item)
					{
						if (('string' == gettype($item)) && (false !== mb_strpos($item, $param, 0,"utf-8")) && ($count < (integer)$N))
						{
							array_push($found, $subarr);
							$count++;
						} elseif ($count > $N)
						{
							return $found;
							break;
						}
					}
				}
			}
		}	
	}
    catch (exception $e)
    {
    }
    return $found;
}

?>