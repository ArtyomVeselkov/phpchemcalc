#+OPTIONS:    H:3 num:nil toc:3 \n:t ::t |:t ^:{} -:t f:t *:t tex:t d:(HIDE) tags:not-in-toc
#+STARTUP:    align fold nodlcheck oddeven lognotestate hideblocks latexpreview showall
* Project is under private construction

* О программе
  Основная задача =phpChemCalc= -- выполнение простых расчётов кислотности / основности ( водных ) растворов кислот и оснований на основе решения системы двух уравнений ( материальный баланс и электронейтральность ), с учётом автопротлиза растворителя.

* Лицензия
  MIT

* Документация
  Расположена на *GDrive*: [[https://www.googledrive.com/host/0B7G8cgWhXA42ZFZqRFFnM21qRDA/doc]]
