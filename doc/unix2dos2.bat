@echo off
REM keep a counter for files converted
set /A nfile=0
REM walk directory structure and convert each file in quiet mode
for /R %1 %%v in (*.tex, makefile, *.html, *.js, *.txt, *.css, *.log, *.idx, *.out, *.ilg, *.aux, *.toc, *.bat, *.repository, *.md, *.xml, *.xsd, *.3, *.sty, *.xslt) do (
    echo converting "%%~nxv" ...
    D:\pfbin\todos.exe "%%v"
    set /A nfile+=1
)
echo Done! Converted %nfile% file(s)