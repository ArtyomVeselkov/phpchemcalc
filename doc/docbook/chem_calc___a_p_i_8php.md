chemCalc\_API.php File Reference {#chem_calc___a_p_i_8php}
================================

-   const
    [CHEMMODULEAPI](#chem_calc___a_p_i_8php_1abe1ee86632018a795a6bc86080f23faf)

<!-- -->

-   [\$start](#chem_calc___a_p_i_8php_1a50a00e7de77365e00b117e73aa82fb9b)

<!-- -->

-   [\$time](#chem_calc___a_p_i_8php_1a78db1a0602e3b6ac1d9a1b5ec103c160)

<!-- -->

-   [chem\_pH](#chem_calc___a_p_i_8php_1aae3a1b7a95a42c9cf1dcfd98400f31a0)
    ( \$c, \$m, \$f, \$Kw, \$om, \$mi, \$ou, \$Kd1, \$Kd2, \$Kd3, \$e,
    \$cs, \$ce)

<!-- -->

-   [\_pKdfmt](#chem_calc___a_p_i_8php_1a8c26d0e6c146775ce2df56360c2abb3a)
    ( \$Kd, \$s, \$ifN, \$decimals, \$decpoint)

<!-- -->

-   [chem\_searchAcidBaseKd](#chem_calc___a_p_i_8php_1ae3d44f86583cb697bcf98be5ce53afbe)
    ( \$string, \$N, \$reg, \$regexp, \$om, \$style, \$str\_tr,
    \$str\_item, \$str\_1col, \$str\_lastRow)

<!-- -->

-   [chem\_main\_ph](#chem_calc___a_p_i_8php_1a452b0feb3f405af016cd0bc2bad60c65)
    ( & \$str\_log)

<!-- -->

-   [chem\_main\_searchABKd](#chem_calc___a_p_i_8php_1a3f85e5df44e7e4ca4fca0c5e0d05a153)
    ( & \$str\_log)

<!-- -->

-   [chem\_main](#chem_calc___a_p_i_8php_1a4b8ca3a9b392d1616c04e694c671d58a)
    ( )

Definition in file
D:/udata/.ws/.comp/.dev/.code/.svn/phpChemCalc/src/chemCalc\_API.php
