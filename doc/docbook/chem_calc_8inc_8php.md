chemCalc.inc.php File Reference {#chem_calc_8inc_8php}
===============================

-   const
    [CHEM\_API\_ACTION](#chem_calc_8inc_8php_1ad1e845104d1f2e471f73a8f0cb29044b)

<!-- -->

-   const
    [CHEM\_API\_PH](#chem_calc_8inc_8php_1a799c4ceb70e23ba56e7bf5bf85e1c8dc)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD](#chem_calc_8inc_8php_1a448bed0d24df074c13975a4e6c733ba2)

<!-- -->

-   const
    [CHEM\_API\_PH\_CONCENTRATION](#chem_calc_8inc_8php_1a4d0d8bd5219b233a29448e9b26120126)

<!-- -->

-   const
    [CHEM\_API\_PH\_MODE](#chem_calc_8inc_8php_1a2242f25295af2d3af1f2a76f56bf9fb1)

<!-- -->

-   const
    [CHEM\_API\_PH\_FORMULA](#chem_calc_8inc_8php_1afcc2746bc359388aa5598851fb879419)

<!-- -->

-   const
    [CHEM\_API\_PH\_KW](#chem_calc_8inc_8php_1a4d97612fb264e6608d787d429686c556)

<!-- -->

-   const
    [CHEM\_API\_PH\_OUTPUTMODE](#chem_calc_8inc_8php_1a998d7fd0a47f5dff7ec75acd482f117f)

<!-- -->

-   const
    [CHEM\_API\_PH\_MAXITER](#chem_calc_8inc_8php_1a5b5a00466888a167fb656452c420c3d5)

<!-- -->

-   const
    [CHEM\_API\_PH\_KD1](#chem_calc_8inc_8php_1a38416c0f381d79802ce9df342fa24f91)

<!-- -->

-   const
    [CHEM\_API\_PH\_KD2](#chem_calc_8inc_8php_1ae48e4d5cda79d3531905fce1523bbebe)

<!-- -->

-   const
    [CHEM\_API\_PH\_KD3](#chem_calc_8inc_8php_1ac0c84760c57b064858a52a35ada396fd)

<!-- -->

-   const
    [CHEM\_API\_PH\_USEOWN](#chem_calc_8inc_8php_1a625a13ef449b3b93f75bb7b984f2d43a)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_STRING](#chem_calc_8inc_8php_1a4ae6f316122970bfc0c5f2906174c4e2)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_MAXN](#chem_calc_8inc_8php_1add235471fcf362274855e2523293ac80)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_CASESEN](#chem_calc_8inc_8php_1ae2e06ffc102689c9e8a69783ce7d490b)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_REGEXP](#chem_calc_8inc_8php_1ad1e522a8a80ee80a97500a53fc34bb71)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_OUTPUTMODE](#chem_calc_8inc_8php_1a5a4b2fcbc7fccdffbfb3b089b1abcaa6)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_STYLE](#chem_calc_8inc_8php_1a3e3c73caa5a04961ee37100306ea917b)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_TDITEM\_TEXT](#chem_calc_8inc_8php_1a7009fd91d0bbec50413f053efc2a4a00)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_TRITEM\_ATTR](#chem_calc_8inc_8php_1a438c64ea471f0dc78c8c0064fc0d6008)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_TRDESCRCOL\_TEXT](#chem_calc_8inc_8php_1a504912df34a4597e8937089cf71b11d3)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_TRLASTTEXT](#chem_calc_8inc_8php_1a956465d0e4f2a748c715dee21b350a8c)

<!-- -->

-   const
    [CHEM\_API\_ARRAY\_TABLE](#chem_calc_8inc_8php_1ab84bf23e87c015828b49797ca2eb2b08)

<!-- -->

-   const
    [CHEM\_API\_ARRAY\_SOURCES](#chem_calc_8inc_8php_1a521b80b6f8ebe0974929d60ef7d7b8aa)

<!-- -->

-   const
    [CHEM\_API\_ARRAY\_LANG\_RUS](#chem_calc_8inc_8php_1ad6bda2200e3b420ab46b0c3c506344a8)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_NAME](#chem_calc_8inc_8php_1a4274e3ff90eb78bfe3aba7fb258bfc22)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_FORMULA](#chem_calc_8inc_8php_1abaa85051408aed72dbf362c70b54e81a)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_TEMP](#chem_calc_8inc_8php_1a30e51f617196d5a2e1e0fafeeeb8d498)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_SOURCE](#chem_calc_8inc_8php_1a34262f92db3adc64ed3193b008b07dc8)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_NOTES](#chem_calc_8inc_8php_1ace24acf706dea9b36b1342acf28239d2)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_RESULTS](#chem_calc_8inc_8php_1ad6c57b56b377390e8aa0ebecf7aaa2da)

<!-- -->

-   const
    [CHEM\_API\_SEARCHABKD\_LANG\_NOTALLRESULTS](#chem_calc_8inc_8php_1a62b4b271c19586afa265c978d1dd5687)

<!-- -->

-   [\$chemDB\_tables](#chem_calc_8inc_8php_1ad7c2db6ce9099c565507039e8dd37474)

Definition in file
D:/udata/.ws/.comp/.dev/.code/.svn/phpChemCalc/src/chemCalc.inc.php
