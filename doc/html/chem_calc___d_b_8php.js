var chem_calc___d_b_8php =
[
    [ "array2_search", "chem_calc___d_b_8php.html#ace3fe85cb2d9bba776dadc28a80f05e1", null ],
    [ "chemDB_findAcidBaseKdValue", "chem_calc___d_b_8php.html#ac993d9dc4c5174f58e3601bf6c261eb0", null ],
    [ "chemDB_readAcidBaseKdTable", "chem_calc___d_b_8php.html#aa42328c4bf3646291427651acf50ef9d", null ],
    [ "chemDB_readAcidKdTable", "chem_calc___d_b_8php.html#ac19a072ac0d7cd7fa3b85c30573b88aa", null ],
    [ "chemDB_readBaseKdTable", "chem_calc___d_b_8php.html#a0911ea5bc6f77e7404369b0359e7bbcd", null ],
    [ "chemDB_searchNAcidBaseKdValue", "chem_calc___d_b_8php.html#a32a8d6f6a10f20797af02da0d7525fee", null ],
    [ "readCSV", "chem_calc___d_b_8php.html#a2ce46ab1a717ea7fa95be752235b78ff", null ]
];