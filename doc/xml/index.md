<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygenindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="index.xsd" version="1.8.6">
<compound refid="chem_calc_8inc_8php" kind="file"><name>chemCalc.inc.php</name>
<member refid="chem_calc_8inc_8php_1ad1e845104d1f2e471f73a8f0cb29044b" kind="variable"><name>CHEM\_API\_ACTION</name></member>
<member refid="chem_calc_8inc_8php_1a799c4ceb70e23ba56e7bf5bf85e1c8dc" kind="variable"><name>CHEM\_API\_PH</name></member>
<member refid="chem_calc_8inc_8php_1a448bed0d24df074c13975a4e6c733ba2" kind="variable"><name>CHEM\_API\_SEARCHABKD</name></member>
<member refid="chem_calc_8inc_8php_1a4d0d8bd5219b233a29448e9b26120126" kind="variable"><name>CHEM\_API\_PH\_CONCENTRATION</name></member>
<member refid="chem_calc_8inc_8php_1a2242f25295af2d3af1f2a76f56bf9fb1" kind="variable"><name>CHEM\_API\_PH\_MODE</name></member>
<member refid="chem_calc_8inc_8php_1afcc2746bc359388aa5598851fb879419" kind="variable"><name>CHEM\_API\_PH\_FORMULA</name></member>
<member refid="chem_calc_8inc_8php_1a4d97612fb264e6608d787d429686c556" kind="variable"><name>CHEM\_API\_PH\_KW</name></member>
<member refid="chem_calc_8inc_8php_1a998d7fd0a47f5dff7ec75acd482f117f" kind="variable"><name>CHEM\_API\_PH\_OUTPUTMODE</name></member>
<member refid="chem_calc_8inc_8php_1a5b5a00466888a167fb656452c420c3d5" kind="variable"><name>CHEM\_API\_PH\_MAXITER</name></member>
<member refid="chem_calc_8inc_8php_1a38416c0f381d79802ce9df342fa24f91" kind="variable"><name>CHEM\_API\_PH\_KD1</name></member>
<member refid="chem_calc_8inc_8php_1ae48e4d5cda79d3531905fce1523bbebe" kind="variable"><name>CHEM\_API\_PH\_KD2</name></member>
<member refid="chem_calc_8inc_8php_1ac0c84760c57b064858a52a35ada396fd" kind="variable"><name>CHEM\_API\_PH\_KD3</name></member>
<member refid="chem_calc_8inc_8php_1a625a13ef449b3b93f75bb7b984f2d43a" kind="variable"><name>CHEM\_API\_PH\_USEOWN</name></member>
<member refid="chem_calc_8inc_8php_1a4ae6f316122970bfc0c5f2906174c4e2" kind="variable"><name>CHEM\_API\_SEARCHABKD\_STRING</name></member>
<member refid="chem_calc_8inc_8php_1add235471fcf362274855e2523293ac80" kind="variable"><name>CHEM\_API\_SEARCHABKD\_MAXN</name></member>
<member refid="chem_calc_8inc_8php_1ae2e06ffc102689c9e8a69783ce7d490b" kind="variable"><name>CHEM\_API\_SEARCHABKD\_CASESEN</name></member>
<member refid="chem_calc_8inc_8php_1ad1e522a8a80ee80a97500a53fc34bb71" kind="variable"><name>CHEM\_API\_SEARCHABKD\_REGEXP</name></member>
<member refid="chem_calc_8inc_8php_1a5a4b2fcbc7fccdffbfb3b089b1abcaa6" kind="variable"><name>CHEM\_API\_SEARCHABKD\_OUTPUTMODE</name></member>
<member refid="chem_calc_8inc_8php_1a3e3c73caa5a04961ee37100306ea917b" kind="variable"><name>CHEM\_API\_SEARCHABKD\_STYLE</name></member>
<member refid="chem_calc_8inc_8php_1a7009fd91d0bbec50413f053efc2a4a00" kind="variable"><name>CHEM\_API\_SEARCHABKD\_TDITEM\_TEXT</name></member>
<member refid="chem_calc_8inc_8php_1a438c64ea471f0dc78c8c0064fc0d6008" kind="variable"><name>CHEM\_API\_SEARCHABKD\_TRITEM\_ATTR</name></member>
<member refid="chem_calc_8inc_8php_1a504912df34a4597e8937089cf71b11d3" kind="variable"><name>CHEM\_API\_SEARCHABKD\_TRDESCRCOL\_TEXT</name></member>
<member refid="chem_calc_8inc_8php_1a956465d0e4f2a748c715dee21b350a8c" kind="variable"><name>CHEM\_API\_SEARCHABKD\_TRLASTTEXT</name></member>
<member refid="chem_calc_8inc_8php_1ab84bf23e87c015828b49797ca2eb2b08" kind="variable"><name>CHEM\_API\_ARRAY\_TABLE</name></member>
<member refid="chem_calc_8inc_8php_1a521b80b6f8ebe0974929d60ef7d7b8aa" kind="variable"><name>CHEM\_API\_ARRAY\_SOURCES</name></member>
<member refid="chem_calc_8inc_8php_1ad6bda2200e3b420ab46b0c3c506344a8" kind="variable"><name>CHEM\_API\_ARRAY\_LANG\_RUS</name></member>
<member refid="chem_calc_8inc_8php_1a4274e3ff90eb78bfe3aba7fb258bfc22" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_NAME</name></member>
<member refid="chem_calc_8inc_8php_1abaa85051408aed72dbf362c70b54e81a" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_FORMULA</name></member>
<member refid="chem_calc_8inc_8php_1a30e51f617196d5a2e1e0fafeeeb8d498" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_TEMP</name></member>
<member refid="chem_calc_8inc_8php_1a34262f92db3adc64ed3193b008b07dc8" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_SOURCE</name></member>
<member refid="chem_calc_8inc_8php_1ace24acf706dea9b36b1342acf28239d2" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_NOTES</name></member>
<member refid="chem_calc_8inc_8php_1ad6c57b56b377390e8aa0ebecf7aaa2da" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_RESULTS</name></member>
<member refid="chem_calc_8inc_8php_1a62b4b271c19586afa265c978d1dd5687" kind="variable"><name>CHEM\_API\_SEARCHABKD\_LANG\_NOTALLRESULTS</name></member>
<member refid="chem_calc_8inc_8php_1ad7c2db6ce9099c565507039e8dd37474" kind="variable"><name>$chemDB_tables</name></member>  </compound>  <compound refid="chem_calc___a_p_i_8php" kind="file"><name>chemCalc_API.php</name>  <member refid="chem_calc___a_p_i_8php_1abe1ee86632018a795a6bc86080f23faf" kind="variable"><name>CHEMMODULEAPI</name></member>  <member refid="chem_calc___a_p_i_8php_1a50a00e7de77365e00b117e73aa82fb9b" kind="variable"><name>$start</name></member>
<member refid="chem_calc___a_p_i_8php_1a78db1a0602e3b6ac1d9a1b5ec103c160" kind="variable"><name>\$time</name></member>
<member refid="chem_calc___a_p_i_8php_1aae3a1b7a95a42c9cf1dcfd98400f31a0" kind="function"><name>chem\_pH</name></member>
<member refid="chem_calc___a_p_i_8php_1a8c26d0e6c146775ce2df56360c2abb3a" kind="function"><name>\_pKdfmt</name></member>
<member refid="chem_calc___a_p_i_8php_1ae3d44f86583cb697bcf98be5ce53afbe" kind="function"><name>chem\_searchAcidBaseKd</name></member>
<member refid="chem_calc___a_p_i_8php_1a452b0feb3f405af016cd0bc2bad60c65" kind="function"><name>chem\_main\_ph</name></member>
<member refid="chem_calc___a_p_i_8php_1a3f85e5df44e7e4ca4fca0c5e0d05a153" kind="function"><name>chem\_main\_searchABKd</name></member>
<member refid="chem_calc___a_p_i_8php_1a4b8ca3a9b392d1616c04e694c671d58a" kind="function"><name>chem\_main</name></member>
</compound>
<compound refid="chem_calc___d_b_8php" kind="file"><name>chemCalc\_DB.php</name>
<member refid="chem_calc___d_b_8php_1a2ce46ab1a717ea7fa95be752235b78ff" kind="function"><name>readCSV</name></member>
<member refid="chem_calc___d_b_8php_1ace3fe85cb2d9bba776dadc28a80f05e1" kind="function"><name>array2\_search</name></member>
<member refid="chem_calc___d_b_8php_1aa42328c4bf3646291427651acf50ef9d" kind="function"><name>chemDB\_readAcidBaseKdTable</name></member>
<member refid="chem_calc___d_b_8php_1a0911ea5bc6f77e7404369b0359e7bbcd" kind="function"><name>chemDB\_readBaseKdTable</name></member>
<member refid="chem_calc___d_b_8php_1ac19a072ac0d7cd7fa3b85c30573b88aa" kind="function"><name>chemDB\_readAcidKdTable</name></member>
<member refid="chem_calc___d_b_8php_1ac993d9dc4c5174f58e3601bf6c261eb0" kind="function"><name>chemDB\_findAcidBaseKdValue</name></member>
<member refid="chem_calc___d_b_8php_1a32a8d6f6a10f20797af02da0d7525fee" kind="function"><name>chemDB\_searchNAcidBaseKdValue</name></member>
</compound>
<compound refid="chem_calc__p_h_8php" kind="file"><name>chemCalc\_pH.php</name>
<member refid="chem_calc__p_h_8php_1a24fab36f6022bcbe3ace2815849523b8" kind="function"><name>calc\_Quadratic</name></member>
<member refid="chem_calc__p_h_8php_1ab5c095a19ddc413497699e333f0d67c0" kind="function"><name>chemCalc\_concentrationHOH\_commonF</name></member>
<member refid="chem_calc__p_h_8php_1a0119a4368040ac5d10ece860ee406ef1" kind="function"><name>chemCalc\_OstwaldDilution</name></member>
<member refid="chem_calc__p_h_8php_1a644987bff8933793f6c2d2f5c25a4693" kind="function"><name>chemCalc\_concentrationHOH\_simplifiedF</name></member>
</compound>
<compound refid="chem_calc___r_u_8php" kind="file"><name>chemCalc\_RU.php</name>
</compound>
<compound refid="dir_68267d1309a1af8e8297ef4c3efbcdba" kind="dir"><name>src</name>
</compound> </doxygenindex>
