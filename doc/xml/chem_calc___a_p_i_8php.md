chemCalc\_API.php
const
const CHEMMODULEAPI
CHEMMODULEAPI
'Chem API defined'
ChemModule: Chemical API.

optimLight 2013 :TODO:use global variables such as \$AcidBaseKdTable
...when read data from file - convert all numbers into corresponding
typessafe string \$str\_tr\_item, \$str\_start\_item,
\$str\_start\_cell, \$str\_start\_end and all other strings from
POSTshow time and RAM usage

\$start
\$start
= microtime(true)
chemCalc\_concentrationHOH\_commonF
\$time
\$time
= microtime(true) - \$start
chemCalc\_concentrationHOH\_commonF
chem\_pH
(\$c, \$m, \$f, \$Kw=1.0E-13, \$om=0, \$mi=150, \$ou=0, \$Kd1=0.0,
\$Kd2=0.0, \$Kd3=0.0, \$e=1.0E-15, \$cs=1.0E-24, \$ce=1.0E+03)
chem\_pH
\$c
\$m
\$f
\$Kw
1.0E-13
\$om
0
\$mi
150
\$ou
0
\$Kd1
0.0
\$Kd2
0.0
\$Kd3
0.0
\$e
1.0E-15
\$cs
1.0E-24
\$ce
1.0E+03
General API used to calculate concentration of \$H\_{3}O\^{+}\$ and
\$OH\^{-}\$ ions.

Direct access not recommended

mixed \$c concentration mixed \$m choose equation, by which "pH" will be
calculated mixed \$f formula or name that uniquely identifies substance
mixed \$log output string for additional information, should be empty
integer \$Kw constant - autoprotolysis integer \$om short from
"OutputMode" - Ajax (1) or HTML (0) retuned data type integer \$mi
number of maimum iterations (if required by equation) integer \$e
accuracy (if required by equation) integer \$cs start from this
concentration (if required by equation) integer \$ce maximum
concentration (if required by equation) type depends on \$om param

\$chemDB\_tables
CHEM\_API\_ARRAY\_TABLE
CHEM\_API\_PH
chemCalc\_concentrationHOH\_commonF
chemCalc\_concentrationHOH\_simplifiedF
chemDB\_findAcidBaseKdValue
chemDB\_readAcidBaseKdTable
chem\_main\_ph
\_pKdfmt
(\$Kd, \$s=0, \$ifN= '-', \$decimals=2, \$decpoint= '.')
\_pKdfmt
\$Kd
\$s
0
\$ifN
'-'
\$decimals
2
\$decpoint
'.'
Format of Kd and pKd output.

mixed \$Kd dissociation constant integer \$s style (0-5) string \$if N
if \$Kd == 0 this string will be used, otherwise \$Kd will be used
integer \$nDigits maximum number of digits after comma :TODO: string

chem\_searchAcidBaseKd
chem\_searchAcidBaseKd
(\$string, \$N, \$reg, \$regexp, \$om, \$style, \$str\_tr= '',
\$str\_item= '', \$str\_1col= '', \$str\_lastRow= '')
chem\_searchAcidBaseKd
\$string
\$N
\$reg
\$regexp
\$om
\$style
\$str\_tr
''
\$str\_item
''
\$str\_1col
''
\$str\_lastRow
''
Returns string with formated data from database about substance which
were identified by send \$param and other settings.

string \$string full or part of namem formula or other data in string
format where \$param appears integer \$N maximum number of retuned items
in array bool \$reg use or not case-sensetive search. ?use if \$RegExp
== TRUE? :TODO: bool \$regexp use RegExp :TODO: integer \$om short from
"OutputMode" - Ajax (1) or HTML (0) retuned data type integer \$style
specified style of retuned string string \$str\_tr string in "html
format" for attributes. This string will be added into each line in the
retuned table string \$str\_item string on each line in the first colomn
(additional param) string \$str\_1col string in the first colomn (extra
colomn will be added) string \$str\_lastRow extra last row in the
retuned table string

\$chemDB\_tables
\_pKdfmt
CHEM\_API\_ARRAY\_LANG\_RUS
CHEM\_API\_ARRAY\_TABLE
CHEM\_API\_PH
CHEM\_API\_SEARCHABKD
CHEM\_API\_SEARCHABKD\_LANG\_FORMULA
CHEM\_API\_SEARCHABKD\_LANG\_NAME
CHEM\_API\_SEARCHABKD\_LANG\_NOTES
CHEM\_API\_SEARCHABKD\_LANG\_RESULTS
CHEM\_API\_SEARCHABKD\_LANG\_SOURCE
CHEM\_API\_SEARCHABKD\_LANG\_TEMP
chemDB\_readAcidBaseKdTable
chemDB\_searchNAcidBaseKdValue
chem\_main\_searchABKd
chem\_main\_ph
(&\$str\_log=null)
chem\_main\_ph
&
\$str\_log
null
CHEM\_API\_PH\_CONCENTRATION
CHEM\_API\_PH\_FORMULA
CHEM\_API\_PH\_KD1
CHEM\_API\_PH\_KD2
CHEM\_API\_PH\_KD3
CHEM\_API\_PH\_KW
CHEM\_API\_PH\_MAXITER
CHEM\_API\_PH\_MODE
CHEM\_API\_PH\_OUTPUTMODE
CHEM\_API\_PH\_USEOWN
chem\_pH
chem\_main
chem\_main\_searchABKd
(&\$str\_log=null)
chem\_main\_searchABKd
&
\$str\_log
null
CHEM\_API\_SEARCHABKD\_CASESEN
CHEM\_API\_SEARCHABKD\_MAXN
CHEM\_API\_SEARCHABKD\_OUTPUTMODE
CHEM\_API\_SEARCHABKD\_REGEXP
CHEM\_API\_SEARCHABKD\_STRING
CHEM\_API\_SEARCHABKD\_STYLE
CHEM\_API\_SEARCHABKD\_TDITEM\_TEXT
CHEM\_API\_SEARCHABKD\_TRDESCRCOL\_TEXT
CHEM\_API\_SEARCHABKD\_TRITEM\_ATTR
CHEM\_API\_SEARCHABKD\_TRLASTTEXT
chem\_searchAcidBaseKd
chem\_main
chem\_main
()
chem\_main
CHEM\_API\_ACTION
CHEM\_API\_PH
CHEM\_API\_SEARCHABKD
chem\_main\_ph
chem\_main\_searchABKd
    <?php

    /******************************************************************************
    *Copyright(c)2013,ArtyomVeselkov
    *
    *ThisfileispartofthephpChemCalc
    *
    *Permissionisherebygranted,freeofcharge,toanypersonobtainingacopy
    *ofthissoftwareandassociateddocumentationfiles(the"Software"),to
    *dealintheSoftwarewithoutrestriction,includingwithoutlimitationthe
    *rightstouse,copy,modify,merge,publish,distribute,sublicense,and/or
    *sellcopiesoftheSoftware,andtopermitpersonstowhomtheSoftwareis
    *furnishedtodoso,subjecttothefollowingconditions:
    *
    *Theabovecopyrightnoticeandthispermissionnoticeshallbeincludedin
    *allcopiesorsubstantialportionsoftheSoftware.
    *
    *THESOFTWAREISPROVIDED"ASIS",WITHOUTWARRANTYOFANYKIND,EXPRESSOR
    *IMPLIED,INCLUDINGBUTNOTLIMITEDTOTHEWARRANTIESOFMERCHANTABILITY,
    *FITNESSFORAPARTICULARPURPOSEANDNONINFRINGEMENT.INNOEVENTSHALLTHE
    *AUTHORSORCOPYRIGHTHOLDERSBELIABLEFORANYCLAIM,DAMAGESOROTHER
    *LIABILITY,WHETHERINANACTIONOFCONTRACT,TORTOROTHERWISE,ARISING
    *FROM,OUTOFORINCONNECTIONWITHTHESOFTWAREORTHEUSEOROTHERDEALINGS
    *INTHESOFTWARE.
    *
    *(Copyright(c)2013,АртёмВеселков
    *
    *Этотфайл—частьphpChemCalc
    *
    *Даннаялицензияразрешаетлицам,получившимкопиюданногопрограммного
    *обеспеченияисопутствующейдокументации(вдальнейшемименуемыми
    *«ПрограммноеОбеспечение»),безвозмездноиспользоватьПрограммное
    *Обеспечениебезограничений,включаянеограниченноеправона
    *использование,копирование,изменение,добавление,публикацию,
    *распространение,сублицензированиеи/илипродажукопийПрограммного
    *Обеспечения,такжекакилицам,которымпредоставляетсяданноеПрограммное
    *Обеспечение,присоблюденииследующихусловий:
    *
    *Указанноевышеуведомлениеобавторскомправеиданныеусловиядолжны
    *бытьвключенывовсекопииилизначимыечастиданногоПрограммного
    *Обеспечения.
    *
    *ДАННОЕПРОГРАММНОЕОБЕСПЕЧЕНИЕПРЕДОСТАВЛЯЕТСЯ«КАКЕСТЬ»,БЕЗКАКИХ-ЛИБО
    *ГАРАНТИЙ,ЯВНОВЫРАЖЕННЫХИЛИПОДРАЗУМЕВАЕМЫХ,ВКЛЮЧАЯ,НОНЕ
    *ОГРАНИЧИВАЯСЬГАРАНТИЯМИТОВАРНОЙПРИГОДНОСТИ,СООТВЕТСТВИЯПОЕГО
    *КОНКРЕТНОМУНАЗНАЧЕНИЮИОТСУТСТВИЯНАРУШЕНИЙПРАВ.НИВКАКОМСЛУЧАЕ
    *АВТОРЫИЛИПРАВООБЛАДАТЕЛИНЕНЕСУТОТВЕТСТВЕННОСТИПОИСКАМОВОЗМЕЩЕНИИ
    *УЩЕРБА,УБЫТКОВИЛИДРУГИХТРЕБОВАНИЙПОДЕЙСТВУЮЩИМКОНТРАКТАМ,ДЕЛИКТАМ
    *ИЛИИНОМУ,ВОЗНИКШИМИЗ,ИМЕЮЩИМПРИЧИНОЙИЛИСВЯЗАННЫМСПРОГРАММНЫМ
    *ОБЕСПЕЧЕНИЕМИЛИИСПОЛЬЗОВАНИЕМПРОГРАММНОГООБЕСПЕЧЕНИЯИЛИИНЫМИ
    *ДЕЙСТВИЯМИСПРОГРАММНЫМОБЕСПЕЧЕНИЕМ.)
    *****************************************************************************/

    //forincludingchemCalc_*.phpscripts
    define('CHEMMODULEAPI','ChemAPIdefined');

    require_once('chemCalc_pH.php');
    require_once('chemCalc_OB.php');
    require_once('chemCalc_DB.php');
    require_once('chemCalc.inc.php');

    functionchem_pH($c,$m,$f,$Kw=1.0E-13,$om=0,$mi=150,$ou=0,$Kd1=0.0,$Kd2=0.0,$Kd3=0.0,$e=
    1.0E-15,$cs=1.0E-24,$ce=1.0E+03)
    {
    //ifdon'tuseownKdorsetted$Kdis<=0
    if((0==$ou)||(0>=$Kd1))
    {
    global$chemDB_tables;
    $table=chemDB_readAcidBaseKdTable($chemDB_tables[CHEM_API_PH][CHEM_API_ARRAY_TABLE]);
    $data=chemDB_findAcidBaseKdValue($f,$table);
    if($data!=(-1))
    {
    $Kd1=(float)$data[3];//!!!oruse&$Kd1...
    $Kd2=(float)$data[4];
    $Kd3=(float)$data[5];
    }else
    return(-1);
    }
    //otherwiseusesend$Kdvalues
    $log='';
    $result=0==$m?chemCalc_concentrationHOH_commonF($c,$Kd1,$Kd2,$Kd3,$log,$Kw,$e,$mi,$cs,$ce):
    chemCalc_concentrationHOH_simplifiedF($c,$Kd1,$Kw);
    switch($om)
    {
    case0:
    {
    echo(-1)!=$result?'[H<sub>3</sub>O<sup>+</sup>]='.$result.',pH='.(-1)*log($result,10).
    '|[OH<sup>-</sup>]='.($Kw/$result).',pOH='.(-1)*log(($Kw/$result),10):'';
    return(-1)!=$result?0:(-1);
    }
    }
    return(-1);
    }

    function_pKdfmt($Kd,$s=0,$ifN='-',$decimals=2,$decpoint='.')
    {
    $K=(float)$Kd;
    if((''!=$ifN)&&(0==$Kd))
    {
    return$ifN;
    }
    switch($s)
    {
    case0:
    {
    returnnumber_format(((-1)*log($K,10)),$decimals,$decpoint);
    }
    case1:
    {
    return'pK='.number_format((-1)*log($K,10),$decimals,$decpoint);
    }
    case2:
    {
    return'pK='.number_format((-1)*log($K,10),$decimals,$decpoint).'|K='.number_format($K,$decimals,$decpoint);
    }
    case3:
    {
    returnnumber_format($K,$decimals,$decpoint);
    }
    case4:
    {
    return'K='.number_format($K,$decimals,$decpoint);
    }
    case5:
    {
    returnsprintf('K=%.'.(string)$decimals.'e'.'|pK=%01.'.(string)$decimals.'f',$K,((-1)*log($K,10)));
    }
    }
    }

    functionchem_searchAcidBaseKd($string,$N,$reg,$regexp,$om,$style,$str_tr='',$str_item='',$str_1col='',$str_lastRow=
    '')
    {
    if((strlen($string)<2)||(strlen($string)>40))
    {
    return(-1);
    }
    global$chemDB_tables;
    $table=chemDB_readAcidBaseKdTable($chemDB_tables[CHEM_API_PH][CHEM_API_ARRAY_TABLE]);
    $amount=0;
    $result=chemDB_searchNAcidBaseKdValue($string,$table,$N,$amount,$reg,$regexp);
    if(-1==$result)
    {
    return(-1);
    }
    $str='';
    if($amount>0)
    {
    switch($om)
    {
    case0:
    {
    //$str_iteminsteadof$str_start_cellwasusedhereforeasyviewing
    //:TODO:add<td>Notes</td>
    //:WARNING:Useclassonly,notidfortags
    $str='<tableclass="chemapi_abkdTable"><trclass="chemapi_abkdDesc">'.
    (''!=$str_item?'<tdclass="chemapi_abkdCol1">'.$str_1col.'</td>':'').
    '<td>'.
    $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_NAME].
    '</td><td>'.
    $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_FORMULA].
    '</td><td>'.
    $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_TEMP].',<sup>0</sup>C'.
    '</td><td>Kd1</td>'.
    '<td>Kd2</td>'.
    '<td>Kd3</td>'.
    '<td>Kd4</td><td>'.
    $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_SOURCE].
    '</td><td>'.
    $chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_NOTES].
    '</td></tr>';
    $i=0;
    for($i=0;$i<$amount;$i++)
    {
    $str.='<tr'.$str_tr.'>'.(''!=$str_item?'<tdclass="chemapi_abkdCol1TD">'.$str_item.'</td>':'').//startcolomn
    '<td>'.$result[$i][0].//name
    '</td><td><spanclass="f">'.$result[$i][1].//fromula
    '</span></td><td>'.$result[$i][2].//temperature
    '</td><td>'._pKdfmt($result[$i][3],$style).//Kd1
    '</td><td>'._pKdfmt($result[$i][4],$style).//Kd2
    '</td><td>'._pKdfmt($result[$i][5],$style).//Kd3
    '</td><td>'._pKdfmt($result[$i][6],$style).//Kd4
    '</td><td>'.$result[$i][7].//Source
    //'</td><td>'.$result[$i][8].//Notes
    '</td></tr>';
    }
    $str.=(''!=$str_lastRow?'<trclass="chemapi_abkdLRow"><td>'.$str_lastRow.'<spanclass="chemapi_abkdLog"'.sprintf('%d$s%d',$amount,$chemDB_tables[CHEM_API_SEARCHABKD][CHEM_API_ARRAY_LANG_RUS][CHEM_API_SEARCHABKD_LANG_RESULTS],$N).'</span></td></tr>':'').'</table>';
    echo$str;
    return0;
    }
    }
    }
    return(-1);
    }

    functionchem_main_ph(&$str_log=null)
    {
    $c=isset($_POST[CHEM_API_PH_CONCENTRATION])?$_POST[CHEM_API_PH_CONCENTRATION]:0;
    $f=isset($_POST[CHEM_API_PH_FORMULA])?$_POST[CHEM_API_PH_FORMULA]:'';
    //$c=((FALSE==is_float($c))&&(FALSE==is_int($c)))?0:$c;
    //$f=htmlspecialchars($_POST[CHEM_API_PH_FORMULA]);
    if((0==$c)||(strlen($f)<3))
    {
    if(null!=$str_log)
    {
    $str_log.='Erroroccurred-notspecifiedconcentrationorformula.';
    }else
    {
    //$exit_str.'Erroroccurred-notspecified$cor$f.';
    }
    return(-1);
    }
    $Kw=isset($_POST[CHEM_API_PH_KW])?$_POST[CHEM_API_PH_KW]:1E-13;
    $m=isset($_POST[CHEM_API_PH_MODE])?$_POST[CHEM_API_PH_MODE]:0;
    $om=isset($_POST[CHEM_API_PH_OUTPUTMODE])?$_POST[CHEM_API_PH_OUTPUTMODE]:0;
    $mi=isset($_POST[CHEM_API_PH_MAXITER])?$_POST[CHEM_API_PH_MAXITER]:150;
    $uo=isset($_POST[CHEM_API_PH_USEOWN])?$_POST[CHEM_API_PH_USEOWN]:0;
    if(0!=$uo)
    {
    $Kd1=$_POST[CHEM_API_PH_KD1];
    $Kd2=$_POST[CHEM_API_PH_KD2];
    $Kd3=$_POST[CHEM_API_PH_KD3];
    $Kd1=((false==is_float(Kd1))&&(false==is_int(Kd1)))?0:Kd1;
    if(0==$Kd1){
    $str_log.='NotvalidvalueforKd1.';
    return(-1);
    }
    }
    //$E,$CSand$CEcanbedefault
    return(0==$uo)?chem_pH($c,$m,$f,$Kw,$om,$mi):chem_pH($c,$m,$f,$Kw,$om,$mi,$ou,$Kd1,$Kd2,$Kd3);
    }

    functionchem_main_searchABKd(&$str_log=null)
    {
    $str=isset($_POST[CHEM_API_SEARCHABKD_STRING])?$_POST[CHEM_API_SEARCHABKD_STRING]:'';
    $n=isset($_POST[CHEM_API_SEARCHABKD_MAXN])?$_POST[CHEM_API_SEARCHABKD_MAXN]:10;
    $r=isset($_POST[CHEM_API_SEARCHABKD_CASESEN])?$_POST[CHEM_API_SEARCHABKD_CASESEN]:FALSE;
    $re=isset($_POST[CHEM_API_SEARCHABKD_REGEXP])?$_POST[CHEM_API_SEARCHABKD_REGEXP]:false;
    $om=isset($_POST[CHEM_API_SEARCHABKD_OUTPUTMODE])?$_POST[CHEM_API_SEARCHABKD_OUTPUTMODE]:0;
    $s=isset($_POST[CHEM_API_SEARCHABKD_STYLE])?$_POST[CHEM_API_SEARCHABKD_STYLE]:5;
    $str_it=isset($_POST[CHEM_API_SEARCHABKD_TDITEM_TEXT])?stripcslashes($_POST[CHEM_API_SEARCHABKD_TDITEM_TEXT]):'';
    $str_tr=isset($_POST[CHEM_API_SEARCHABKD_TRITEM_ATTR])?stripcslashes($_POST[CHEM_API_SEARCHABKD_TRITEM_ATTR]):'';
    $str_c=isset($_POST[CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT])?stripcslashes($_POST[CHEM_API_SEARCHABKD_TRDESCRCOL_TEXT]):'';
    $str_e=isset($_POST[CHEM_API_SEARCHABKD_TRLASTTEXT])?stripcslashes($_POST[CHEM_API_SEARCHABKD_TRLASTTEXT]):'';

    if((strlen($str)<2)||(strlen($str)>40))
    {
    $str_log.='.Lengthofsearchedstringmustbe2-40symbols';
    return(-1);
    }
    returnchem_searchAcidBaseKd($str,$n,$r,$re,$om,$s,$str_tr,$str_it,$str_c,$str_e);
    //returnchem_searchAcidBaseKd($str,10,false,false,0,5);
    }

    functionchem_main()
    {
    $str_log='Functionstarts';
    $action=isset($_POST[CHEM_API_ACTION])?$_POST[CHEM_API_ACTION]:'';
    if(''==$action)
    {
    echo$str_log.'.Functionstops.Accessdenied.';
    return(-1);
    }
    switch($action)
    {
    caseCHEM_API_PH:
    {
    $str_log.='>CHEM_API_PH';
    (-1)!=chem_main_ph($str_log)?$str_log.=':Resultwerereturned.Noerrorsoccurred.':$str_log.=
    ':Erroroccurred.Badresultdata';
    break;
    }

    caseCHEM_API_SEARCHABKD:
    {
    $str_log.='>CHEM_API_SEARCHABKD';
    (-1)!=chem_main_searchABKd($str_log)?$str_log.=':Resultwerereturned.Noerrorsoccurred.':$str_log.=
    ':Erroroccurred.Badresultdata';
    break;
    }
    }
    return'';
    //$str_log.'.Functionends.';
    }

    $start=microtime(true);
    chem_main();
    $time=microtime(true)-$start;
    //echo'<br/>Timeusage:'.$time;

    ?>
        
